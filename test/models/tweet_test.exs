defmodule Hinatter.TweetTest do
  use Hinatter.ModelCase

  alias Hinatter.Tweet

  @valid_attrs %{meta: %{}}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Tweet.changeset(%Tweet{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Tweet.changeset(%Tweet{}, @invalid_attrs)
    refute changeset.valid?
  end
end
