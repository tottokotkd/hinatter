use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :hinatter, Hinatter.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :hinatter, Hinatter.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "test_user",
  password: "test",
  database: "hinatter_test_db",
  hostname: "127.0.0.1",
  port: 5431,
  pool: Ecto.Adapters.SQL.Sandbox
