# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :hinatter,
  ecto_repos: [Hinatter.Repo]

# Configures the endpoint
config :hinatter, Hinatter.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Gos8l54MtXxufFlyI1hvO0o/7HPy164+2QjouRQSVWrRryGhAMBUxDTNuMt+ENsf",
  render_errors: [view: Hinatter.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Hinatter.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"


# twitter
config :extwitter, :oauth, [
   consumer_key: "lcAHeipAoarJg32VricXDyUxP",
   consumer_secret: "mI5L0SVi1QUBBwvANrIO7SQkY746nU5XMRfG4ZRgN8iFNKMGck",
   access_token: "110440023-9nNr8atn20cQXjL9PbVxYtzWkJR4x1NbgcRcv5oc",
   access_token_secret: "rBFodJYcdj4XNNkhE06Wv773XjlsayjyGCgVO4uGL54uo"
]
