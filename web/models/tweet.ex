defmodule Hinatter.Tweet do
  use Hinatter.Web, :model

  schema "tweets" do
    field :meta, :map

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:meta])
    |> validate_required([:meta])
    |> unique_constraint(:meta, name: :required_tweet_id)
    |> unique_constraint(:meta, name: :tweets__meta__id__TEXT_index)
    |> unique_constraint(:meta, name: :tweets_meta_JSONB_PATH_OPS_index)
  end

  def test() do
    for tweet <- ExTwitter.search("比奈", [count: 100]) do
      map =  Map.from_struct(tweet)
      result = Hinatter.Tweet.changeset(%Hinatter.Tweet{}, %{meta: map}) |> Hinatter.Repo.insert

      case result do
        {:ok, pomodoro_todo} ->
          {:ok, pomodoro_todo}
        {:error, changeset} ->
          IO.inspect changeset
          {:error, changeset}
      end
    end
  end
end
