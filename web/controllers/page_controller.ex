defmodule Hinatter.PageController do
  use Hinatter.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
