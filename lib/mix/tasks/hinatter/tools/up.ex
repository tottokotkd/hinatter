defmodule Mix.Tasks.Hinatter.Tools.Up do
  use Mix.Task

  @moduledoc "set up dev. tools for hinatter"

  def run(_args) do
    Mix.shell.cmd "docker-compose -f docker/devTools.yml up -d"
  end

end
