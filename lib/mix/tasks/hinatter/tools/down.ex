defmodule Mix.Tasks.Hinatter.Tools.Down do
  use Mix.Task

  @moduledoc "set up dev. tools for hinatter"

  def run(_args) do
    Mix.shell.cmd "docker-compose -f docker/devTools.yml down"
  end

end
