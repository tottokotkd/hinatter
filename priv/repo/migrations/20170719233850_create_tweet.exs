defmodule Hinatter.Repo.Migrations.CreateTweet do
  use Ecto.Migration

  def change do
    create table(:tweets) do
      add :meta, :map, null: false

      timestamps()
    end

    create constraint(:tweets, :required_tweet_id, check: ~s|meta ? 'id'|)
    create unique_index(:tweets, ["((meta->>'id')::TEXT)"])
    create index(:tweets, ["meta JSONB_PATH_OPS"], using: "GIN")
  end
end
